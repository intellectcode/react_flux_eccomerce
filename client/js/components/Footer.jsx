var React = require("react");
var footer = React.createClass({
	render : function(){
		return (
			<div>
    <footer class="footer">
            <div class="container footer-links">
                <div class="center footer-brand">
                    <img src="../images/logo.png"/>
                </div>
                <div class="footer-content">
                    <div class="col-md-4 col-wraper">
                        <ul class="footer-list">
                            <li>
                                <span class="footer-left-menu col-text"><a href="#">ABOUT US</a></span>
                                <span class="footer-left-menu col-text"><a href="#">CONTACT US</a></span>
                                <span class="footer-left-menu col-text"><a href="#">HELP</a></span>
                            </li>
                            <li>
                                <span class="footer-left-menu col-text">Payment Options:</span>
                                <span class="footer-left-menu"><img class="payment-badge-icon" src="../images/cod.png" /></span>
                                <span class="footer-left-menu"><img class="payment-badge-icon" src="../images/visa.png" /></span>
                                <span class="footer-left-menu"><img class="payment-badge-icon" src="../images/master-card.png" /></span>
                            </li>
                        </ul>
                    </div>
                    <div class="center col-md-4 col-wraper">
                        <ul class="footer-list store-info">
                            <li><img class="store-badge-icon" src="../images/play-badge.png" /></li>
                            <li class="col-text">Coming Soon</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-wraper">
                        <ul class="footer-list contact-info">
                            <li><img class="footer-right-icons" src="../images/call.png"/>Call Us at {{adminContactInfo.phoneNumber}}</li>
                            <li><img class="footer-right-icons" src="../images/whatsapp.png"/>WhatsApp Us at {{adminContactInfo.whatsAppNumber}}</li>
                            <li><img class="footer-right-icons" src="../images/email.png"/>Email Us at {{adminContactInfo.email}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
</div>
		)
	}
})