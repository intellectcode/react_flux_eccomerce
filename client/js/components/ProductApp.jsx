var React = require("react");
var ProductStore = require("../stores/ProductStore");
var ProductActions = require("../actions/ProductActions");

var Products = require("./Products.jsx");
var Cart = require("./Cart.jsx");

function getAppState() {
    return {
        products: ProductStore.getProducts()
    };
}

var Product = React.createClass({
    getInitialState: function() {
        return getAppState();
    },
    componentDidMount: function() {
        this.bindEvents();
        ProductActions.getProducts();
    },
    componentWillUnmount: function() {
       this.removeEvents();
    },
    bindEvents: function(){
        ProductStore.addChangeListener('change', this._onChange);
        ProductStore.addChangeListener('test', this._onTest);
    },
    removeEvents: function(){
        ProductStore.removeChangeListener('change', this._onChange);
        ProductStore.removeChangeListener('test', this._onTest);
    },
    _onChange: function() {
        this.setState(getAppState());
    },
    _onTest: function() {
        console.log("on test evt");
    },
    render: function() {
        return(
            // <div>
            //     {this.state.showProductComponent ? <Products products={this.state.products} productsIncart={this.state.productsIncart} showComponent={this.state.showProductComponent}/> : ""}
            //     {this.state.showCartComponent ? <Cart products={this.state.productsIncart} showComponent={this.state.showCartComponent}/> : "" }
            // </div>

             <div class="container product-container">
                <div class="row center product-container-title-row">
                    <div class="col-md-4">
                        <span class="right"><img src="./images/prism.png" /></span>
                    </div>
                    <div class="col-md-4">
                        <span class="product-container-title">Fruits</span>
                    </div>
                    <div class="col-md-4">
                        <span class="left"><img src="./images/prism.png" /></span>
                    </div>
                </div>
                <div class="row product-row" >
                    <div class="col-md-3 card" ng-repeat="item in fruit_products" ng-if="item.category== 'fruit'" ng-init="priceQtyObj[item.name] = item.priceQuantityList[0];selectedDropDownVal = item.priceQuantityList[0].priceUnit">
                        <div class="product-card center">
                            <a href="javascript:void(0)" id= "{{item._id}}" ng-click="viewProductDetails(item, $event)"><img class="product-image" alt="{{item.name}}" title="{{item.name}}" ng-src="{{item.productImage[0] || './images/tomato.png' }}" />
                            <p>{{item.name }}</p></a>
<!--                            <p class="strike">Rs {{item.priceQuantityList[0].oldPrice}} per Kg</p>-->
                            
                            <p class="strike">Rs {{priceQtyObj[item.name].oldPrice}} {{priceQtyObj[item.name].priceUnit}}</p>
                            
                            <p class="currentPrice">Rs {{priceQtyObj[item.name].newPrice}}<span> {{priceQtyObj[item.name].priceUnit}}</span></p>
                            <select required="required" class="product-quantity quantity" ng-model="selectedDropDownVal" ng-change="setPriceQtyVal(item,item.name, selectedDropDownVal)">
                                <option ng-repeat="unit in item.priceQuantityList" value="{{unit.priceUnit}}">
                                    {{unit.priceUnit}}
                                </option>
                            </select>
                            <br>
                            <span style="font-weight: bold">Qty</span><input class="chose-quantity" type="number" ng-model="qty" value="1" min="1" max="50"/><br>
                            <button class="add-to-bag" ng-click="addToCart(item,$event,selectedDropDownVal,qty)" >
                            <img src="./images/whitebag.png" /><span>Add to Bag</span>
                            </button>
                        </div><br>  
                    </div>
                    
                <div class="row">
                    <span class="right col-text">
                    <a href="javascript:void(0)" ng-click="goToViewAll('fruit')" class="fruit-more-link">View All</a>
                    </span>
                </div>
            </div>
        )
    }
});

module.exports = Product;