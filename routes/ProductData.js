var categories = {
    "Men": {
        "t_shirt": "Tshirt",
        "jeans": "Jeans"
    },
    "Women": {
        "legging": "Legging",
        "kurti": "Kurti"
    }
}

var static_content = {
    "home_banner": ""
}

var products = [{
    category_id: "Men",
    name: "",
    price: ""
}]



module.exports.categories = categories;
module.exports.static_content = static_content;
module.exports.products = products;
